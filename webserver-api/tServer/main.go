package main

import (
	"fmt"
	"log"
	"net/http"
	"strings"
	"time"
)

type THandler int

func (tHandler THandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if strings.HasSuffix(r.URL.Path, ".js") {
		tServeJs(w, r)
	} else if strings.HasSuffix(r.URL.Path, ".css") {
		tServeCss(w, r)
	} else if strings.HasSuffix(r.URL.Path, ".svg") {
		tServeSvg(w, r)
	} else if strings.HasSuffix(r.URL.Path, ".html") {
		tServeHtml(w, r)
	} else if strings.HasSuffix(r.URL.Path, "/") {
		tServeHtml(w, r)
	} else if strings.HasSuffix(r.URL.Path, ".ico") {
		tServeIco(w, r)
	} else if strings.HasSuffix(r.URL.Path, ".webp") {
		tServeWebp(w, r)
	} else if strings.HasSuffix(r.URL.Path, ".jpeg") {
		tServeJpeg(w, r)
	} else if strings.HasSuffix(r.URL.Path, ".png") {
		tServePng(w, r)
	} else if strings.HasSuffix(r.URL.Path, ".pdf") {
		tServePdf(w, r)
	} else if strings.HasSuffix(r.URL.Path, ".pptx") {
		tServePptx(w, r)
	} else if strings.HasSuffix(r.URL.Path, "/v") {
		fmt.Fprint(w, "Server 0.0")
	}
}

func tServeJs(w http.ResponseWriter, r *http.Request) {
	// Connection
	w.Header().Set("Cache-Control", "no-cache")
	// Message
	w.Header().Set("Content-Type", "text/javascript; charset=utf-8")
	w.Header().Set("Content-Language", "pt-br")
	// To be handled by webapp
	w.Header().Set("key-Code", "00000000001")
	//Serve Files
	http.ServeFile(w, r, "js/"+r.URL.Path)
}

func tServeCss(w http.ResponseWriter, r *http.Request) {
	// Connection
	w.Header().Set("Cache-Control", "no-cache")
	// Message
	w.Header().Set("Content-Type", "text/css; charset=utf-8")
	// To be handled by webapp
	w.Header().Set("key-Code", "00000000001")
	//Serve Files
	http.ServeFile(w, r, "css/"+r.URL.Path)
}

func tServeSvg(w http.ResponseWriter, r *http.Request) {
	// Connection
	w.Header().Set("Cache-Control", "no-cache")
	// Message
	w.Header().Set("Content-Type", "image/svg+xml; charset=utf-8")
	w.Header().Set("Content-Language", "pt-br")
	// To be handled by webapp
	w.Header().Set("key-Code", "00000000001")
	//Serve File
	http.ServeFile(w, r, "svg/"+r.URL.Path)
}

func tServeHtml(w http.ResponseWriter, r *http.Request) {
	// Protocol
	w.Header().Set("HTTP-Version", "HTTP/2.0") //Chrome OK
	w.Header().Set("X-Firefox-Spdy", "h2")     //Chrome OK
	w.Header().Set("Protocol", "HTTP/2.0")     //Chrome OK
	// Connection
	w.Header().Set("Cache-Control", "no-cache") //Chrome OK
	w.Header().Set("Date", time.Now().Format(time.UnixDate))
	// Message
	w.Header().Set("Content-Type", "text/html; charset=utf-8") //Chrome OK
	w.Header().Set("Content-Language", "pt-br")                //Chrome OK
	// To be handled by webapp
	w.Header().Set("key-Code", "00000000001")

	//Serve File
	if strings.Contains(r.Host, "nofrete.com") {
		http.ServeFile(w, r, "no-frete/index.html")
	} else if strings.Contains(r.Host, "audiomedicus.com") {
		http.ServeFile(w, r, "audio-medicus/index.html")
	} else if strings.Contains(r.Host, "naporteira.com") {
		http.ServeFile(w, r, "na-porteira/index.html")
	} else if strings.Contains(r.Host, "noluar.com") {
		http.ServeFile(w, r, "no-luar/index.html")
	} else if strings.Contains(r.Host, "tcarvi.com") {
		http.ServeFile(w, r, "tcarvi/index.html")
	}

}

func tServeIco(w http.ResponseWriter, r *http.Request) {
	// Message
	w.Header().Set("Content-Type", "image/x-icon")
	// To be handled by webapp
	w.Header().Set("key-Code", "00000000001")
	//Serve File
	if strings.Contains(r.Host, "nofrete.com") {
		http.ServeFile(w, r, "no-frete/ico/"+r.URL.Path)
	} else if strings.Contains(r.Host, "audiomedicus.com") {
		http.ServeFile(w, r, "audio-medicus/ico/"+r.URL.Path)
	} else if strings.Contains(r.Host, "naporteira.com") {
		http.ServeFile(w, r, "na-porteira/ico/"+r.URL.Path)
	} else if strings.Contains(r.Host, "noluar.com") {
		http.ServeFile(w, r, "no-luar/ico/"+r.URL.Path)
	} else if strings.Contains(r.Host, "tcarvi.com") {
		http.ServeFile(w, r, "tcarvi/ico/"+r.URL.Path)
	}
}

func tServeWebp(w http.ResponseWriter, r *http.Request) {
	// Message
	w.Header().Set("Content-Type", "image/webp")
	// To be handled by webapp
	w.Header().Set("key-Code", "00000000001")
	//Serve Files
	http.ServeFile(w, r, "webp/"+r.URL.Path)
}

func tServeJpeg(w http.ResponseWriter, r *http.Request) {
	// Message
	w.Header().Set("Content-Type", "image/jpeg")
	// To be handled by webapp
	w.Header().Set("key-Code", "00000000001")
	//Serve Files
	http.ServeFile(w, r, "jpeg/"+r.URL.Path)
}

func tServePng(w http.ResponseWriter, r *http.Request) {
	// Message
	w.Header().Set("Content-Type", "image/png")
	// To be handled by webapp
	w.Header().Set("key-Code", "00000000001")
	//Serve Files
	http.ServeFile(w, r, "png/"+r.URL.Path)
}

func tServePdf(w http.ResponseWriter, r *http.Request) {
	// Message
	w.Header().Set("Content-Type", "application/pdf")
	// To be handled by webapp
	w.Header().Set("key-Code", "00000000001")
	//Serve Files
	http.ServeFile(w, r, "pdf/"+r.URL.Path)
}

func tServePptx(w http.ResponseWriter, r *http.Request) {
	// Message
	w.Header().Set("Content-Type", "application/vnd.mspowerpoint")
	// To be handled by webapp
	w.Header().Set("key-Code", "00000000001")
	//Serve Files
	http.ServeFile(w, r, "pptx/"+r.URL.Path)
}

// TODO: func database() string
// 	db, err := sql.Open("postgres", "user=postgres dbname=np sslmode=disable")

// TODO: func answer(q Question) Answer
// a, err := ai.Answers(q)

func main() {
	var tHandler THandler
	err := http.ListenAndServe(":8080", tHandler)
	// TODO HTTPS ACCESS: log.Fatal(srv.ListenAndServeTLS(certFile, keyFile string))
	if err != nil {
		log.Fatal(err)
	}
}
