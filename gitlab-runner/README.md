# Configuração para Gitlab Runner

- Stages
  - Each stage contains one or many job
  - The jobs of different stages are executed in a linear sequence
  - Jobs of the same stage are executed in the same time
  - One stage only is started when the later is finished successifuly
  - Examples of stages:
    - Build
      - Job: build
    - Test
      - Jobs: code_quality, container_scanning, dependency_scanning, license_management, sast, test
    - Production
      - Job: production
    - Performance
      - Job: performance

#### About images and services:
- [Gitlab documentation about Docker Images](https://docs.gitlab.com/ee/ci/docker/using_docker_images.html)
- [Gitlab documentation about services](https://docs.gitlab.com/ee/ci/docker/using_docker_images.html#what-is-a-service)
- Exemplo:  
```  
before_script:  
  - docker info  
  
build:  
  stage: Build  
    image: eduardoleal1981/go-server:latest  
  services:  
  - postgres:9.3  
```  
  
#### About Gitlab Runner
- *Link*: https://docs.gitlab.com/runner/
- *Open source project*
- It is used to run jobs and send the results back to GitLab.
- It is used in conjunction with GitLab CI, the open-source continuous integration service included with GitLab that coordinates the jobs.
- GitLab Runner requires a minimum of Docker v1.13.0.
- Allows to run:
  - Multiple jobs concurrently.
  - Use multiple tokens with multiple server (even per-project).
  - Limit number of concurrent jobs per-token.
- Jobs can be run:
  - Locally.
  - **Using Docker containers.**
  - Using Docker containers and executing job over SSH.
  - Using Docker containers with autoscaling on different clouds and virtualization hypervisors.
  - Connecting to remote SSH server.
- Is written in Go and distributed as single binary without any other requirements.
- Supports Bash, Windows Batch, and Windows PowerShell.
- Works on GNU/Linux, macOS, and Windows (pretty much anywhere you can run Docker).
- Allows customization of the job running environment.
- Automatic configuration reload without restart.
- Easy to use setup with support for Docker, Docker-SSH, Parallels, or SSH running environments.
- Enables caching of Docker containers.
- Easy installation as a service for GNU/Linux, macOS, and Windows.
- Embedded Prometheus metrics HTTP server.
- Referee workers to monitor and pass Prometheus metrics and other job-specific data to GitLab
- The GitLab Runner version should be in sync with the GitLab version.

- Installation:
  - The official way of deploying a GitLab Runner instance into your Kubernetes cluster is by using the ***gitlab-runner Helm chart***.
  - This chart configures the Runner to:
    - ***Run using the GitLab Runner Kubernetes executor***.
    - ***For each new job it receives from GitLab CI/CD, it will provision a new pod within the specified namespace to run it.***
- Prerequisites:
  - Your GitLab Server’s API is reachable from the cluster.
  - Kubernetes 1.4+ with Beta APIs enabled.
  - The kubectl CLI installed locally and authenticated for the cluster.
  - The Helm client installed locally on your machine.

- Configuração do GitLab Runner:
  - Create a values.yaml file for your GitLab Runner configuration. See Helm docs for information on how your values file will override the defaults.
  - The default configuration can always be found in the values.yaml in the chart repository.

- Registro do GitLab
  - Once GitLab Runner is installed, you need to register it with GitLab.
  - Learn how to register a GitLab Runner