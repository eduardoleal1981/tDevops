# Google Cloud

#### Fluxo de criação/execução de cluster Kubernetes
- Acessar projeto
- Acessar tópico **Kubernetes Engine**
- Usar opção de criação de cluster. Configurá-lo conforme documentação atualizada.
    - https://cloud.google.com/kubernetes-engine/docs/concepts/cluster-architecture?hl=pt_BR
- Deve-ser ter a seguinte estrutura de diretórios e arquivos:
    - Repositórios *git*:
        - /home/eduardoleal1981/audio-medicus
        - /home/eduardoleal1981/na-porteira
        - /home/eduardoleal1981/no-frete
        - /home/eduardoleal1981/no-luar
        - /home/eduardoleal1981/tcarvi
        - demais projetos web ...
    - Arquivo /home/eduardoleal1981/.docker/config.json
    - ```
        {
            "credHelpers": {
                "gcr.io": "gcloud"
            }
        }```
    - Arquivos de apresentação das versões do *container*:
        - buildVERSION
        - minorVERSION
        - majorVERSION
        - VERSION
    - *Scripts* para alteração automatizada das versões:
        - buildVersioning.sh
        - minorVersioning.sh
        - majorVersioning.sh
    - *Script* automatizador de *build* e *deploy*:
        - updateTcarvi.sh
- execute o script `./updateTcarvi.sh`:
```
# For first time cluster configuration:  
# gcloud container clusters get-credentials cluster-1 --zone southamerica-east1-a --project ${PROJECT_ID}  
  
export PROJECT_ID=tcarvi  
gcloud config set project $PROJECT_ID  
  
# Initial clone  
# git clone https://gitlab-ci-token:XXXTOKENXXX@gitlab.com/eduardoleal1981/audio-medicus.git  
# git clone https://gitlab-ci-token:XXXTOKENXXX@gitlab.com/eduardoleal1981/na-porteira.git  
# git clone https://gitlab-ci-token:XXXTOKENXXX@gitlab.com/eduardoleal1981/no-frete.git  
# git clone https://gitlab-ci-token:XXXTOKENXXX@gitlab.com/eduardoleal1981/no-luar.git  
# git clone https://gitlab-ci-token:XXXTOKENXXX@gitlab.com/eduardoleal1981/tcarvi.git  
  
# Pull projects  
cd audio-medicus && git pull && cd ..  
cd na-porteira && git pull && cd ..  
cd no-frete && git pull && cd ..  
cd no-luar && git pull && cd ..  
cd tcarvi && git pull && cd ..  
  
# Docker configuration. (apenas primeiro uso do docker)  
# sudo groupadd --system docker  
# sudo chgrp docker ./*.*  
# sudo chmod 540 buildVersioning.sh  
# sudo chmod 540 minorVersioning.sh  
# sudo chmod 540 majorVersioning.sh  
# sudo chmod 540 updateTcarvi.sh  
  
# Exclusão de imagem antiga e de imagems criadas indevidamente  
OLD_VERSION_NUMBERS="$(cat VERSION)"  
docker rmi -f gcr.io/tcarvi/tcarvi-webserver:$OLD_VERSION_NUMBERS || true  
# clean untagged images  
docker system prune --force  
  
# Versioning  
# ./majorVersioning.sh  
# ./minorVersioning.sh  
./buildVersioning.sh  
  
VERSION_NUMBERS="$(cat VERSION)"  
  
# Build  
docker build --rm --file tcarvi/devops/docker/Dockerfile --tag gcr.io/${PROJECT_ID}/tcarvi-webserver:${VERSION_NUMBERS} .  
# clean untagged images  
docker system prune --force  
# For first time docker registry configuration:  
# gcloud auth configure-docker  
# push the image  
docker push gcr.io/${PROJECT_ID}/tcarvi-webserver:${VERSION_NUMBERS}  
  
# Verifique se há instâncias de cluster em execução:  
gcloud compute instances list  
  
# For first time publication:  
# O Kubernetes representa aplicativos como pods, que são unidades que representam um contêiner, ou grupo de contêineres estreitamente associados.  
# O pod é a menor unidade implantável no Kubernetes.  
# Use o comando kubectl para criar um deployment. O Kubernetes criará uma implantação chamada tcarvi-server-deploy no cluster.  
# Comando executado apenas na primeira publicação:  
# kubectl create deployment tcarvi-server-deploy --image=gcr.io/${PROJECT_ID}/tcarvi-webserver:${VERSION_NUMBERS}  
  
# Verifique se a implantação foi criada corretamente, em um pod:  
kubectl get pods  
  
# Exponha a implantação recém criada na internet  
# Para primeira vez, execute comando completo:  
# A sinalização --port especifica o número da porta configurada no balanceador de carga, e a sinalização --target-port especifica o número da porta em que o contêiner tcarvi-webserver está escutando.  
# kubectl expose deployment tcarvi-server-deploy --type=LoadBalancer --port 80 --target-port 8080  
  
# Verifique características da publicação:  
kubectl get service  
  
# Se desejar, aumente a performance de execução dos pods com replica  
# O balanceador de carga provisionado em etapa anterior começará a direcionar o tráfego para essas novas réplicas automaticamente.  
# kubectl scale deployment tcarvi-server-deploy --replicas=3  
  
# Verifique características do deploy, inclusive das replicas:  
kubectl get deployment tcarvi-server-deploy  
# Verifique novamente a execução dos pods:  
# kubectl get pods  
  
# Execute atualização do container devido versionamento da imagem publicada  
kubectl set image deployment/tcarvi-server-deploy tcarvi-webserver=gcr.io/${PROJECT_ID}/tcarvi-webserver:${VERSION_NUMBERS}  
```
#### Versionamentos:
- buildVersioning.sh 
```
#!/bin/bash  
majorVersion="$(cat majorVERSION)"  
minorVersion="$(cat minorVERSION)"  
buildVersion="$(cat buildVERSION)"  
((buildVersion++))  
echo "Old version: $(cat VERSION)"  
echo "$buildVersion" > buildVERSION  
echo "$majorVersion:$minorVersion:$buildVersion" > VERSION  
echo "New version $(cat VERSION)"  
```  
- minorVersioning.sh 
```
majorVersion="$(cat majorVERSION)"  
minorVersion="$(cat minorVERSION)"  
((minorVersion++))  
buildVersion="$(cat buildVERSION)"  
echo "Old version: $(cat VERSION)"  
echo "$minorVersion" > minorVERSION  
echo "$majorVersion:$minorVersion:$buildVersion" > VERSION  
echo "New version $(cat VERSION)"  
```  
- majorVersioning.sh 
```
#!/bin/bash  
majorVersion="$(cat majorVERSION)"  
((majorVersion++))  
minorVersion="$(cat minorVERSION)"  
buildVersion="$(cat buildVERSION)"  
echo "Old version: $(cat VERSION)"  
echo "$majorVersion" > majorVERSION  
echo "$majorVersion:$minorVersion:$buildVersion" > VERSION  
echo "New version $(cat VERSION)"  
``` 

#### Aplicações instaláveis em cluster Kubernetes
- [Helm](https://github.com/helm/helm)
     - Site: [https://helm.sh/docs/](https://helm.sh/docs/)
    - Helm is a tool for managing Charts. Charts are packages of pre-configured Kubernetes resources.
    - Use Helm to:
        - Find and use popular software packaged as Helm Charts to run in Kubernetes
        - Share your own applications as Helm Charts
        - Create reproducible builds of your Kubernetes applications
        - Intelligently manage your Kubernetes manifest files
        - Manage releases of Helm packages
- Ingress
    - An API object that manages external access to the services in a cluster, typically HTTP.
    - Ingress can provide load balancing, SSL termination and name-based virtual hosting.
- Cert-Manager
    - It is a native Kubernetes certificate management controller. It can help with issuing certificates from a variety of sources, such as Let’s Encrypt, HashiCorp Vault, Venafi, a simple signing key pair, or self signed.
    - It will ensure certificates are valid and up to date, and attempt to renew certificates at a configured time before expiry.
- Prometheus
    - An open-source systems monitoring and alerting toolkit
    - Main features:
        - a multi-dimensional data model with time series data identified by metric name and key/value pairs
        - PromQL, a flexible query language to leverage this dimensionality
        - no reliance on distributed storage; single server nodes are autonomous
        - time series collection happens via a pull model over HTTP
        - pushing time series is supported via an intermediary gateway
        - targets are discovered via service discovery or static configuration
        - multiple modes of graphing and dashboarding support
- GitLab Runner
    - An open source project that is used to run your jobs and send the results back to GitLab. It is used in conjunction with GitLab CI, the open-source continuous integration service included with GitLab that coordinates the jobs.
    - Allows to run:
        - Multiple jobs concurrently.
        - Use multiple tokens with multiple server (even per-project).
        - Limit number of concurrent jobs per-token.
        - Jobs can be run:
            - Locally.
            - Using Docker containers.
            - Using Docker containers and executing job over SSH.
            - Using Docker containers with autoscaling on different clouds and virtualization hypervisors.
            - Connecting to remote SSH server.
        - Is written in Go and distributed as single binary without any other requirements.
        - Supports Bash, Windows Batch, and Windows PowerShell.
        - Works on GNU/Linux, macOS, and Windows (pretty much anywhere you can run Docker).
        - Allows customization of the job running environment.
        - Automatic configuration reload without restart.
        - Easy to use setup with support for Docker, Docker-SSH, Parallels, or SSH running environments.
        - Enables caching of Docker containers.
        - Easy installation as a service for GNU/Linux, macOS, and Windows.
        - Embedded Prometheus metrics HTTP server.
        - Referee workers to monitor and pass Prometheus metrics and other job-specific data to GitLab.

#### Administração de cluster Kubernetes
- gcloud
    - Manage Google Cloud Platform resources and developer workflow
    - Most important command lines:
        - `gcloud app` - manage your App Engine deployments
        - `gcloud auth` - manage oauth2 credentials for the Google Cloud SDK
        - `gcloud builds` - create and manage builds for Google Cloud Build
        - `gcloud components` - list, install, update, or remove Google Cloud SDK components
        - `gcloud compute` - create and manipulate Google Compute Engine resources
        - `gcloud compute zones list | grep southamerica` - list compute zones which has "southamerica" in its name.
        - `gcloud config` - view and edit Cloud SDK properties
        - `gcloud config set compute/zone southamerica-east1-a` - Config the gcloud commands to use compute zone.
        - `gcloud container` - deploy and manage clusters of machines for running containers
        - `gcloud deployment-manager` - manage deployments of cloud resources
        - `gcloud dns` - manage your Cloud DNS managed-zones and record-sets
        - `gcloud docker` - enable Docker CLI access to Google Container Registry
        - `gcloud domains` - manage domains for your Google Cloud projects
        - `gcloud endpoints` - create, enable and manage API services
        - `gcloud functions` - manage Google Cloud Functions
        - `gcloud help` - search gcloud help text
        - `gcloud info` - display information about the current gcloud environment
        - `gcloud init` - initialize or reinitialize gcloud
        - `gcloud iot` - manage Cloud IoT resources
        - `gcloud kms` - manage cryptographic keys in the cloud
        - `gcloud ml` - use Google Cloud machine learning capabilities
        - `gcloud ml-engine` - manage AI Platform jobs and models
        - `gcloud run` - manage your Cloud Run applications
        - `gcloud scheduler` - manage Cloud Scheduler jobs and schedules
        - `gcloud services` - list, enable and disable APIs and services
        - `gcloud source` - cloud git repository commands
        - `gcloud version` - print version information for Cloud SDK components
- kubctl
    - Control Kubernetes clusters
    - Configuration files inside `$HOME/.kube` directory.
    - Sintax: `kubectl [command] [TYPE] [NAME] [flags]`
    - [Examples documentation](https://kubernetes.io/docs/reference/kubectl/overview/)
    - [Reference documentation](https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands)
    - [Reference for executing docker commands](https://kubernetes.io/docs/reference/kubectl/docker-cli-to-kubectl/
- [Other tools](https://kubernetes.io/docs/reference/tools/)
    - `kubeadm` is the command line tool for easily provisioning a secure Kubernetes cluster on top of physical or cloud servers or virtual machines (currently in alpha)
    - `kubefed` is the command line tool to help you administrate your federated clusters.
    - `minikube` is a tool that makes it easy to run a single-node Kubernetes cluster locally on your workstation for development and testing purposes.
    - `Dashboard`, the web-based user interface of Kubernetes, allows you to deploy containerized applications to a Kubernetes cluster, troubleshoot them, and manage the cluster and its resources itself.
    - `Kubernetes Helm` is a tool for managing packages of pre-configured Kubernetes resources, aka Kubernetes charts.
    - `Kompose` is a tool to help Docker Compose users move to Kubernetes.

#### WEB console CLI for Kubernetes cluster:
- Access: `https://console.cloud.google.com/`
- Definir um projeto como padrão, para receber comandos do gcloud:
    - `gcloud config set project no-luar`
- Definir a região do projeto:
    - `gcloud config set compute/region southamerica-east1`
- Definir a zona do projeto:
    - `gcloud config set compute/zone southamerica-east1-b`
- Atualizar o gcloud:
    - `gcloud components update`
- Criar um cluster, para posterior criação de containers e do fluxo DEVOPS:
    - Link: https://cloud.google.com/sdk/gcloud/reference/container/clusters/create?hl=pt_BR
    - Comando: `gcloud container clusters create cluster-no-luar --region=southamerica-east1 --zone southamerica-east1-b`   
        - Possíveis parâmetros: 
            ```
                --image-type=debian-10-buster-v20200204 \  
                --enable-autorepair \  
                --enable-autoupgrade \  
                --enable-vertical-pod-autoscaling \  
                --image-type=debian-10-buster-v20200204 \  
                --issue-client-certificate \  
                --network=NETWORK \  
                --node-version=NODE_VERSION \  
                --num-nodes=NUM_NODES  
            ```
- Conectar com cluster:
    - `gcloud container clusters get-credentials noluar-cluster-1 --zone southamerica-east1-a --project no-luar`

- Deploy de imagem, já existente no registry gcr:
    - `kubectl create deployment hello-web --image=gcr.io/${PROJECT_ID}/hello-app:v1`
    - `kubectl expose deployment hello-web --type=LoadBalancer --port 80 --target-port 8080`

- Atualização de deploy de imagem versionada:
    - git pull
    - build e push de imagem versionada
    - `kubectl set image deployment/hello-web hello-app=gcr.io/${PROJECT_ID}/hello-app:v2`

#### Referências:
- https://cloud.google.com/kubernetes-engine/docs/tutorials?hl=pt_BR