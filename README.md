# Execução local no Windows
- `rm -r C:\libs\go\src\tServer` 
- `cp -r tServer C:\libs\go\src`
- `go install tServer`
- `tServer`

# DEVOPS

#### Estrutura de pastas
- gitlab-runner
    - Primeiro Fluxo DEVOPS. Executado quando há entrega no repositório do código-fonte. 
    - Indicação de ***jobs*** individuais ou de ***jobs*** agrupados como ***stages***
    - Indicação de Imagens e Services usados para os ***jobs***
    - Indicação do ***Runner*** usado para o ***build*** e para o ***push*** das imagens "dockerizadas".
    - Ordem para execução de *script* ***build.sh***
- docker
    - Segundo Fluxo DEVOPS.
    - Descrição de imagem em arquivo Dockerfile, usado pelo Gitlab Runner para o *build*.
- gcloud
    - Terceiro Fluxo DEVOPS.
    - Integração do repositório Gitlab com o Gitlab-Runner do cluster Kubernetes.
    - Instruções para push de imagem Docker em registry do cluster Kubernetes
    - Instruções para publicação de imagem Docker em container acessado pelo browser do cliente final, usando domínio DNS do site.

### Script build.sh
- Configurações exigidas pelo sistema cloud do Google
    - export PROJECT_ID=no-frete
    - export VERSION=0.0.1
- Tag exigida pelo sistema cloud da Google, para comando ***docker build***:
    - *-t gcr.io/${PROJECT_ID}/nofrete:${VERSION}*